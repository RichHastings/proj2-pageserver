# README # 
 
## Author: Rich Hastings, ghastings@uoregon.edu ##

Intro for Dockers . CIS 322, Introduction to Software Engineering, at the University of Oregon. 

Basic web server will return file contents if file exists. 
Otherwise it will return 404 error.
if requested file contains "..", "//"", or "~"  will return 403 error.