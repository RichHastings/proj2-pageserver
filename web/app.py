from flask import Flask
from flask import render_template

#
app = Flask(__name__)

@app.route("/<path:url>")
def hello(url):
    if '~' in url or '..' in url or '//' in url or url=="/":
        return render_template('403.html'), 403
    else:
        try:
           render_template(url)
        except:
            return render_template('404.html'), 404

        return render_template(url)

@app.errorhandler(404)
def not_found(error):
    return ('File not found!'), 404

@app.errorhandler(403)
def forbbin(error):
    return render_template('403.html'), 403


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
